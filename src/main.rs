use std::borrow::BorrowMut;
use std::collections::HashSet;
use std::fs::File;
use std::io::{Error, Read, Write};
use std::path::{Path, PathBuf};
use std::process::{Command, Output};
use std::time::Duration;
use std::{fs, process, thread};

mod xspf;

// LOCAL Directories
const MUSIC_DIR: &str = "/home/roman/Music/";
const PLAYLIST_DIR: &str = "/home/roman/Music/";

// remote Directories
const PHONE_MNT: &str = "/run/user/1000/"; // "random-id/" -> next line
const PHONE_BULK: &str = "storage/3332-6661/";
const PHONE_PL: &str = "storage/emulated/0/BlackPlayer/Playlists/";

// cache already created folders / copied songs
const CACHE_FOLDERS: &str = ".cached_folders";
const CACHE_COPIES: &str = ".cached_copies";

// return kde-connect id (without \n)
fn get_kde_connect_id() -> Result<String, Error> {
    let out = Command::new("kdeconnect-cli")
        .args(["-l", "--id-only"])
        .output();
    match out {
        Ok(id) => Ok(String::from_utf8(id.stdout).unwrap().replace("\n", "")),
        Err(e) => Err(e),
    }
}

fn main() {
    println!("Getting KDE Connect Phone ID...");
    let kde_id = get_kde_connect_id().unwrap();

    // pc/mnt/phoneid/
    let phone_path = format!("{}{}/", PHONE_MNT, kde_id);
    println!("Phone Paths:");
    println!(" -> {:?}", &phone_path);

    // pc/mnt/phoneid/sdcard/musiclibrary
    let phone_path_lib = format!("{}{}", phone_path, PHONE_BULK);
    println!(" -> {:?}", &phone_path_lib);

    // pc/mnt/phoneid/internal/blackplayer/pl
    let phone_path_pl = format!("{}{}", phone_path, PHONE_PL);
    println!(" -> {:?}", &phone_path_pl);

    // pc/music-pl-dir
    let playlist_dir = Path::new(PLAYLIST_DIR);
    println!("Playlist Directory: {:?}", &playlist_dir);

    // TODO
    // ask here wether to continue or not
    thread::sleep(Duration::from_secs(1));

    // acces cache file for folders and get its pre-populated hashmap
    let (mut cachefile_folders, mut cached_dirs) = get_cache_dirs();

    println!("Cached: {:?}", &cached_dirs);

    // get all playlists
    for pl_path in get_playlists(playlist_dir) {
        println!("- Current Playlist: {:?}", pl_path);

        // create new file on remote phone
        let pl_name = pl_path.file_stem().unwrap().to_str().unwrap();
        let pl_path_dst = format!("{}{}.m3uBackup", phone_path_pl, pl_name);
        println!("- Creating New: {}", pl_path_dst);
        let mut playlist_dest = fs::File::create(pl_path_dst).unwrap();

        // create file that rsync/cp all songs for each playlist
        let pl_copy_script_name = format!("pl/{}.sh", pl_name);
        let mut pl_copy_script = fs::File::create(pl_copy_script_name).unwrap();

        // read playlist and decode xspf
        let source_pl_raw = fs::read_to_string(pl_path).unwrap();
        let playlist = xspf::decode(&source_pl_raw).unwrap();

        // loop over all songs in source playlist
        for song in playlist {
            println!("  - Adding {:?}", song.title);
            let path_original = format!("{}{}", MUSIC_DIR, &song.url);
            let path_phone = format!("/{}{}\n", PHONE_BULK, &song.url);

            // write phone path to playlist
            let _ = playlist_dest.write_all(path_phone.as_bytes());

            // write rsync command to sh script in this folder
            // pc/mnt/phoneid/sdcard/musiclibrary/artist/album/song.flac
            let song_dest = format!("{}{}", phone_path_lib, &song.url);
            let command = format!(
                "rsync --ignore-existing --progress -r \"{}\" \"{}\"\n",
                path_original, song_dest
            );
            let _ = pl_copy_script.write_all(command.as_bytes());

            // make folders required to copy songs
            // pc/mnt/phoneid/sdcard/musiclibrary/artist/album/
            let song_folder_name = PathBuf::from(song_dest);
            let song_folder = song_folder_name.parent().unwrap();

            // if song's folder was never cached -> create and add to cache
            if !cached_dirs.contains(song_folder.to_str().unwrap()) {
                if !song_folder.exists() {
                    println!("  - Path DOES NOT EXIST: {}", song_folder.to_str().unwrap());
                    match fs::create_dir_all(song_folder) {
                        Ok(_) => {
                            println!("  - Made folder");
                            cached_dirs.insert(song_folder.to_str().unwrap().to_string());
                        }
                        Err(e) => {
                            eprintln!("{:?}", e);
                        }
                    }
                } else {
                    // folder apparently already exists, to speed up future playlist-rs runs, cache
                    // it
                    cached_dirs.insert(song_folder.to_str().unwrap().to_string());
                }
            }
        }
        //process::exit(0);
    }

    // if completed -> write cache to file!()
    let res = write_cache_folders(&mut cachefile_folders, cached_dirs);
    println!("Wrote cached folder to cache: {:?}", res);
}

/// get playlists located on Computer / Music Source
/// get all files in music dir (not files in subdirs)
/// filter out non playlists i.e. not ending in .xspf
fn get_playlists(dir: &Path) -> Vec<PathBuf> {
    let mut playlists = vec![];
    let pl_dir = fs::read_dir(dir).unwrap();
    for pl in pl_dir {
        let pl_path = pl.unwrap().path();

        // ignore files without extensions & extract extension
        let Some(ext) = pl_path.extension() else {
            continue;
        };

        // filter out everything besides xspf/playlists
        if !ext.eq("xspf") {
            println!("ignoring: {:?}", pl_path);
            continue;
        }

        playlists.push(pl_path);
    }

    playlists
}

// get the cache file handle for folders, and extracted hashset
fn get_cache_dirs() -> (File, HashSet<String>) {
    // load cached folders here
    let mut file_cache_folders = match fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(CACHE_FOLDERS)
    {
        Ok(f) => f,
        Err(e) => panic!(
            "Could not open or create cache file for folders: {}\n{}",
            CACHE_FOLDERS, e
        ),
    };

    // read file to string
    let mut cached_dirs = String::new();
    file_cache_folders.read_to_string(&mut cached_dirs).unwrap();

    // create hashset and fill with cache file content
    let mut hashed_cache: HashSet<String> = HashSet::new();
    for cached_folder in cached_dirs.lines() {
        hashed_cache.insert((cached_folder).to_string());
    }

    (file_cache_folders, hashed_cache)
}

// write changed hashset to cachefile
// TODO: diff file and hashset contents, and only write changed lines
//
// BIGMODEIDEA: use extracted hastset as read only
//  -> create empty hashset for writing into
//  -> in the main loop i'd need to write newly created or discovered folders into both hashsets
fn write_cache_folders(
    cachefile_folders: &mut File,
    cached_dirs: HashSet<String>,
) -> Result<(), Error> {
    for hash in cached_dirs.iter() {
        let mut new_hash = hash.to_owned();
        new_hash.push('\n');
        let res = cachefile_folders.write_all(new_hash.as_bytes());
        println!("{:?}", res);
    }

    cachefile_folders.flush()
}
