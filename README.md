# playlist-rs

A simple tool to create music playlists for your phone, from existing playlists on your (Linux) PC.

## Goal

prepare a folder with playlist files and music to simply copy over to phone.
Only the music in the playlists should be copied over, to save storage on your phone.

## music player app

I'm using BlackPlayer (com.musicplayer.blackplayerfree).

## Paths

On my phone, BlackPlayer saves playlist backups to `Internal/Blackplayer/playlist.m3uBackup`
Playlists are normally saved to `unknown?` (hidden dir bc android security?

Music lives on my sdcard: `/storage/3332-6661/`

KDE Connect can auto mount my phone over wifi/eth:
`/run/user/1000/xxxxxxxxx/storage/3332-6661/`

Find your phone's ID with
`kdeconnect-cli -l --id-only`

## Issues

* Does not check if source music file exists (stale playlist paths)
* create put mkdir into script of playlist -> only create folders for the playlist you copy over
* need to "mount" phone in explorer/dolphin first, before running scripts

## Tasks

* allow source_music_lib and destination_music_lib to be specified
    * (currently both are hardcoded "MusikButterTitan")
* cache already created folders
    * access to phone through sshfs is slow (especially file ops)
* playlist selection
* faster partial playlist file copy -> also cache music library on phone locally?

